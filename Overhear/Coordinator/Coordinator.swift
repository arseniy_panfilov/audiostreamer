//
//  Coordinator.swift
//  Overhear
//
//  Created by Arseniy on 14.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import UIKit


class Coordinator {
  
  var window: UIWindow?
  
  init() {
    
  }
  
  func launchMainInterface() {
    let type = "_overhear._tcp."
    let window = UIWindow(frame: UIScreen.main.bounds)
    self.window = window
    
    let browser = ServiceBrowser(type: type)
    
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let root = storyboard.instantiateInitialViewController() as! UITabBarController
    window.rootViewController = root
    window.makeKeyAndVisible()
    _ = root.view
    
    let discoverView = root.viewControllers![0] as! DiscoverViewController
    let discoverPresenter = DiscoverPresenter(browser: browser)
    
    let hostView = root.viewControllers![1] as! HostViewController
    let hostPresenter = HostPresenter(hostManager: AudioHostManager(type: type))
    
    do { // load views
      _ = hostView.view
      _ = discoverView.view
    }
    
    

    hostPresenter.view = hostView
    discoverPresenter.view = discoverView
  }
  
}
