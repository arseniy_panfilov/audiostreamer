//
//  Utils.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation

public func dispatch_in_main_queue(withDelay seconds: Double, block:@escaping ()->()) {
  let dispatchTime = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
  DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: block)
}

extension Optional {
  func useFor(_ block: (Wrapped) throws -> (Void) ) rethrows {
    let _ = try flatMap(block)
  }
}

func bridge<T : AnyObject>(_ obj : T) -> UnsafeMutableRawPointer {
  return UnsafeMutableRawPointer(Unmanaged.passUnretained(obj).toOpaque())
  // return unsafeAddressOf(obj) // ***
}

func bridge<T : AnyObject>(_ ptr : UnsafeRawPointer) -> T {
  return Unmanaged<T>.fromOpaque(ptr).takeUnretainedValue()
  // return unsafeBitCast(ptr, T.self) // ***
}
