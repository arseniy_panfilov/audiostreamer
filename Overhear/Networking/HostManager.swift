//
//  ServiceHost.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import MediaPlayer
import RxCocoa

protocol HostManager {
  func start(_ name: String, description: String)
  func stop()
  
  var clientsArray: Observable<[String]> { get }
}


class AudioHostManager: NSObject, NetServiceDelegate {
  
  var server: NetService?
  let published = Variable(false)
  
  var clients: Observable<[String]> {
    return connected.asObservable().map {
      clients in
      clients.map { $0.identifier.value }
    }
  }
  
  fileprivate let connected = Variable<[ClientHandler]>([])
  
  fileprivate var pendingClients: [ClientHandler] = []
  
  let type: String
  var disposeBag = DisposeBag()
  
  var currentItem: URL?
  
  init(type: String) {
    self.type = type
    super.init()
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: nil) { _ in
      self.server?.publish(options: NetService.Options.listenForConnections)
    }
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: nil) { _ in
      self.server?.stop()
    }

    NotificationCenter.default.addObserver(forName: NSNotification.Name.MPMusicPlayerControllerNowPlayingItemDidChange, object: nil, queue: OperationQueue.main) {
      notification in
      if let currentlyPlaying = MPMusicPlayerController.systemMusicPlayer().nowPlayingItem {
        self.currentItem = currentlyPlaying.assetURL
        self.broadcast()
      }
    }
  }
  
  func broadcast() {
    guard let itemUrl = self.currentItem else {
      return
    }
    for client in connected.value {
      client.outputSream.sendItem(fromUrl: itemUrl)
    }
  }
  
  func start() {
    self.server =  NetService.init(domain: "local.", type: type, name: UIDevice.current.name, port: 0)
    self.server?.includesPeerToPeer = true
    self.server?.delegate = self
    self.server?.publish(options: NetService.Options.listenForConnections)
    MPMusicPlayerController.systemMusicPlayer().beginGeneratingPlaybackNotifications()
  }
  
  func stop() {
    self.server?.stop()
    self.connected.value = []
    self.pendingClients = []
    MPMusicPlayerController.systemMusicPlayer().endGeneratingPlaybackNotifications()
  }
  
  func netService(_ sender: NetService, didNotPublish errorDict: [String : NSNumber]) {
    debugPrint(errorDict)
    assert(sender == self.server)
    assert(false)
  }
  
  func netServiceDidPublish(_ sender: NetService) {
    published.value = true
  }
  
  func urlForPart(_ part: Int) -> URL {
    return Bundle.main.url(forResource: "part\(part)", withExtension: "mp3")!
  }
  
  func netService(_ sender: NetService, didAcceptConnectionWith inputStream: InputStream, outputStream: OutputStream) {
    
    let clientData = ClientHandler(inputStream: inputStream, outputStream: outputStream)
    self.pendingClients.append(clientData)
    
    clientData.disconnected.asObservable().bind { [unowned self] in
      if let index = self.connected.value.index(of: clientData) {
        self.connected.value.remove(at: index)
      }
      clientData.close()
    }.addDisposableTo(clientData.disposeBag)
    
    clientData.identifier.asObservable().bind { [unowned self]
      identifier in
      guard identifier != "" else {
        return
      }
      if let index = self.pendingClients.index(of: clientData) {
        self.pendingClients.remove(at: index)
      }
      self.addConnectedClient(clientData)
    }.addDisposableTo(clientData.disposeBag)
  }
  
  func addConnectedClient(_ client: ClientHandler) {
    self.connected.value.append(client)
    if let currentItem = self.currentItem {
      UIApplication.shared.beginBackgroundTask(withName: "transfer") {
        print("task expired");
      }
      client.outputSream.sendItem(fromUrl: currentItem)
    }
  }
}
