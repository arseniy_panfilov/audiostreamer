//
//  Browser.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import RxSwift

class ServiceBrowser: NSObject, NetServiceBrowserDelegate, StreamDelegate {
  
  fileprivate let type: String
  
  var browser: NetServiceBrowser!
  let services = Variable<[NetService]>([])
  
  let hostName: String? = nil
  
  init(type: String) {
    self.type = type
    super.init()
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: nil) { _ in
      self.browser.searchForServices(ofType: self.type, inDomain: "local.")
    }
    
    NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidEnterBackground, object: nil, queue: nil) { _ in
      self.browser.stop()
    }
  }
  
  func netServiceBrowser(_ browser: NetServiceBrowser, didRemove service: NetService, moreComing: Bool) {
    self.services.value.index(of: service).useFor { self.services.value.remove(at: $0) }
  }
  
  func netServiceBrowser(_ browser: NetServiceBrowser, didFind service: NetService, moreComing: Bool)
  {
    self.services.value.append(service)
  }
  
  func netServiceBrowser(_ browser: NetServiceBrowser, didNotSearch errorDict: [String : NSNumber]) {
    debugPrint(errorDict)
    assert(browser == self.browser)
    assert(errorDict.count > 0)
    assert(false) // catch programming error
  }
  
  func start() {
    self.browser = NetServiceBrowser()
    self.browser.includesPeerToPeer = true
    self.browser.delegate = self
    self.browser.searchForServices(ofType: self.type, inDomain: "local.")
  }
  
  func connectToService(withIndex index: Int) -> AudioClient? {
    return NetworkAudioClient(netService: self.services.value[index])
  }
  
  func stop() {
    self.browser.stop()
    self.browser = nil
    
    self.services.value.removeAll()
  }
}
