//
//  AudioPlayer.swift
//  Overhear
//
//  Created by Arseniy on 17.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer
import RxSwift

class AudioPlayer {
  
  var finished: Observable<Void> { return finishSubject.asObservable() }
  
  let finishSubject = PublishSubject<Void>()
  
  let audioFileStream: AudioFileStream
  
  var autoPlay: Bool
  
  var audioQueue: APAudioQueue?
  
  var volume: Float {
    didSet {
      self.audioQueue?.setVolume(volume)
    }
  }
  
  init(autoPlay: Bool, volume: Float) {
    self.volume = volume
    self.autoPlay = autoPlay
    audioFileStream = AudioFileStream()
    audioFileStream.delegate = self
  }
  
  func play() {
    audioQueue?.play()
    self.autoPlay = true
  }
  
  func finish() {
    audioQueue?.finish()
  }
  
  func parseData(_ data: Data) {
    audioFileStream.parseData(data)
  }
}

extension AudioPlayer: AudioFileStreamDelegate {
  func audioFileStream(_ sender: AudioFileStream, didReceiveError: OSStatus) {
    print("AUDIO ERROR!")
  }
  
  func audioFileStreamDidBecomeReady(_ audioFileStream: AudioFileStream) {
    let bufferSize: Int = (audioFileStream.packetBufferSize > 0) ? Int(audioFileStream.packetBufferSize) : kAPAudioQueueBufferSize
    
    self.audioQueue = APAudioQueue(basicDescription: audioFileStream.basicDescription, bufferCount: kAPAudioQueueBufferCount, bufferSize: bufferSize, magicCookieData: audioFileStream.magicCookieData, magicCookieSize: audioFileStream.magicCookieLength, autoPlay: autoPlay)
    self.audioQueue?.setVolume(self.volume)
    self.audioQueue?.delegate = self
  }
  
  func audioFileStream(_ sender: AudioFileStream, didReceiveData data: Data) {
    self.audioQueue?.fill(withData: data, offset: 0)
  }
  
  func audioFileStream(_ sender: AudioFileStream, didReceiveData data: Data, packetDescription: AudioStreamPacketDescription) {
    self.audioQueue?.fill(withData: data, packetDescription: packetDescription)
  }
}

extension AudioPlayer: APAudioQueueDelegate {
  func audioQueueDidStartPlaying(_ audioQueue: APAudioQueue) {
    //
  }
  
  func audioQueueDidFinishPlaying(_ audioQueue: APAudioQueue) {
    self.finishSubject.on(.next())
  }
}
