//
//  NetworkingConstants.swift
//  Overhear
//
//  Created by Arseniy on 17.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation

class NetworkingConstants {
  static let StopPacket = "stop".data(using: String.Encoding.utf8)!
}
