//
//  AudioInputStreamer.swift
//  Overhear
//
//  Created by Arseniy on 14.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import RxSwift
import MediaPlayer

class AudioInputStreamer: NSObject, StreamDelegate {
  
  fileprivate let inputStream: InputStream
  var data = PublishSubject<Data>()
  
  var bgQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
  
  var currentPlayer: AudioPlayer?
  var nextPlayer: AudioPlayer?
  
  var identifier = Variable<String?>(nil)
  
  fileprivate let identityData = NSMutableData()
  
  var volume: Float = 0.75 {
    didSet {
      self.currentPlayer?.volume = volume
      self.nextPlayer?.volume = volume
    }
  }
  
  fileprivate let disposeBag = DisposeBag()
  
  init(withInputStream inputStream: InputStream) {
    self.inputStream = inputStream
    super.init()
    
    inputStream.delegate = self
    inputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
    inputStream.open()
    
    currentPlayer = createPlayer(true)
  }
  
  func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
    
      let stream = aStream
      switch(eventCode) {
      case Stream.Event.hasBytesAvailable:
        assert(stream == self.inputStream);
        var buffer = [UInt8](repeating: 0, count: kAPAudioStreamReadMaxLength)
        let bytesRead =  self.inputStream.read(&buffer, maxLength: kAPAudioStreamReadMaxLength)
        if (bytesRead <= 0) {
          // TODO: handle
        } else {
          let data = Data.init(bytes: UnsafePointer<UInt8>(buffer), count: bytesRead)
          self.processIncomingData(data)
        }
      default:
        return
      }
    
  }
  
  fileprivate func processIncomingData(_ data: Data) {
    let stopPacketRange = data.range(of: NetworkingConstants.StopPacket, options: [], in: data.range(of: data))
    if let stopRange = stopPacketRange {
      if stopRange.lowerBound > 0 {
        let leftoverRange = Range(uncheckedBounds: (0, stopRange.lowerBound))
        self.currentPlayer?.parseData(data.subdata(in: leftoverRange))
      }
      self.nextPlayer = createPlayer(false)
      self.currentPlayer?.finish()
      if  stopRange.upperBound < data.count {
        let leftoverRange = Range(uncheckedBounds: (stopRange.upperBound, data.count))
        self.nextPlayer?.parseData(data.subdata(in: leftoverRange))
      }
    }
    else {
      let player = nextPlayer ?? currentPlayer
      player?.parseData(data)
    }
  }
  
  fileprivate func createPlayer(_ autoPlay: Bool) -> AudioPlayer {
    let player = AudioPlayer(autoPlay: autoPlay, volume: self.volume)
    player.finished.bind { [unowned self] in
      self.currentPlayer.useFor {
        if player === $0 {
          self.currentPlayer = self.nextPlayer
          self.currentPlayer?.play()
        }
      }
      }.addDisposableTo(disposeBag)
    return player
  }
  
  deinit {
    inputStream.close()
  }
}
