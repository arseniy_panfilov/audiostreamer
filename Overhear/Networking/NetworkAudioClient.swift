//
//  AudioClient.swift
//  TestMCP
//
//  Created by Arseniy on 01.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import UIKit

class NetworkAudioClient: AudioClient {
  
  fileprivate let inputStream: AudioInputStreamer
  fileprivate let outputStream: OutputStream
  let service: NetService
  fileprivate var sentIdentity: Bool = false
  
  init?(netService: NetService) {
    self.service = netService
    
    var inputStream: InputStream?
    var outputStream: OutputStream?
    netService.getInputStream(&inputStream, outputStream: &outputStream)
    
    guard let input = inputStream, let output = outputStream else {
      return nil
    }
    self.outputStream = output
    
    self.inputStream = AudioInputStreamer(withInputStream: input)
    
    self.outputStream.open()
    
    let data = UIDevice.current.name.data(using: String.Encoding.utf8)!
    self.outputStream.write((data as NSData).bytes.bindMemory(to: UInt8.self, capacity: data.count), maxLength: data.count)
    
    let stop = NSData(data: NetworkingConstants.StopPacket)
    self.outputStream.write((stop as NSData).bytes.bindMemory(to: UInt8.self, capacity: stop.length), maxLength: stop.length)
  }
  
  func setVolume(_ volume: Float) {
    self.inputStream.volume = volume
  }
  
  
  deinit {
    self.outputStream.close()
  }
}
