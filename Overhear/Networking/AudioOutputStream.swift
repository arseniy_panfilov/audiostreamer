//
//  AudioOutputStream.swift
//  Overhear
//
//  Created by Arseniy on 14.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer
import RxSwift
import RxCocoa


class AudioOutputStream: NSObject, StreamDelegate {
  
  var assetReader: AVAssetReader!
  var assetOutput: AVAssetReaderTrackOutput!
  
  var bgQueue = DispatchQueue.global(qos: DispatchQoS.QoSClass.background)
  
  var chunksQueue : [Data] = []
  
  let disposeBag = DisposeBag()
  
  fileprivate let outputStream: OutputStream
  
  init(withOutputStream outputStream: OutputStream) {
    self.outputStream = outputStream
    super.init()
    
    outputStream.delegate = self
    outputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.commonModes)
    outputStream.open()
  }
  
  
  func sendChunk() {
    if let chunk = self.chunksQueue.popLast() {
      outputStream.write((chunk as NSData).bytes.bindMemory(to: UInt8.self, capacity: chunk.count), maxLength: chunk.count)
    }
  }
  
  func sendData(_ data: Data) {
    self.chunksQueue.insert(data, at: 0)
  }
  
  
  func sendItem(fromUrl url: URL) {
    DispatchQueue.main.async {
      let asset = AVURLAsset(url: url)
      do {
        self.chunksQueue = []
        self.chunksQueue.append(NSData(data: NetworkingConstants.StopPacket) as Data)
        
        self.assetReader =  try AVAssetReader.init(asset: asset)
        self.assetOutput = AVAssetReaderTrackOutput.init(track: asset.tracks[0], outputSettings: nil)
        
        self.assetReader.add(self.assetOutput)
        assert(self.assetReader.startReading() == true)
        self.populateAssetChunks()
        self.sendChunk()
      }
        
      catch {
        self.assetOutput = nil
        self.assetReader = nil
        return
      }
    }
  }
  
  func populateAssetChunks() {
    while let sampleBuffer = assetOutput.copyNextSampleBuffer() {
      
      var audioBufferList = AudioBufferList()
      
      var blockBuffer: CMBlockBuffer?
      
      CMSampleBufferGetAudioBufferListWithRetainedBlockBuffer(sampleBuffer, nil, &audioBufferList, MemoryLayout<AudioBufferList>.size, nil, nil, UInt32(kCMSampleBufferFlag_AudioBufferList_Assure16ByteAlignment),
                                                              &blockBuffer)
      
      // Create UnsafeBufferPointer from the variable length array starting at audioBufferList.mBuffers
      let audioBuffers = UnsafeBufferPointer<AudioBuffer>(start: &audioBufferList.mBuffers,
                                                          count: Int(audioBufferList.mNumberBuffers))
      
      let size = MemoryLayout<AudioStreamPacketDescription>.size * 100
      var packetsBuffer = [AudioStreamPacketDescription].init(repeating: AudioStreamPacketDescription(), count: 100)
      var neededSize: Int = 0
      CMSampleBufferGetAudioStreamPacketDescriptions(sampleBuffer, size, &packetsBuffer, &neededSize)
      let packetsCount = neededSize/MemoryLayout<AudioStreamPacketDescription>.size
      let total = NSMutableData()
      for audioBuffer in audioBuffers {
        let data = Data.init(bytes: audioBuffer.mData!, count: Int(audioBuffer.mDataByteSize))
        total.append(data)
      }
      for packet in packetsBuffer[0..<packetsCount] {
        let pointer = total.bytes.advanced(by: Int(packet.mStartOffset))
        let packetData = Data(bytes: pointer, count: Int(packet.mDataByteSize))
        chunksQueue.insert(packetData, at: 0)
      }
    }
  }
  
  func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
      
    switch(eventCode) {
      
    case Stream.Event.hasSpaceAvailable:
      self.sendChunk()
    default:
      return
    }
    
  }
  
  deinit {
    self.outputStream.close()
  }
}

