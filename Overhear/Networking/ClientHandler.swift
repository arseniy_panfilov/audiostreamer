//
//  ClientHandler.swift
//  Overhear
//
//  Created by Arseniy on 17.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import RxSwift

class ClientHandler: NSObject, StreamDelegate {
  let inputStream: InputStream
  let outputSream: AudioOutputStream
  
  var disconnected: Observable<Void> { return disconnectSubject.asObservable() }
  fileprivate let disconnectSubject = PublishSubject<Void>()
  
  fileprivate var identity = Data()
  let identifier: Variable<String> = Variable("")
  fileprivate(set) var disposeBag = DisposeBag()
  
  init(inputStream: InputStream, outputStream: OutputStream) {
    self.inputStream = inputStream
    self.outputSream = AudioOutputStream(withOutputStream: outputStream)
    super.init()
    inputStream.open()
    inputStream.delegate = self
    inputStream.schedule(in: RunLoop.current, forMode: RunLoopMode.defaultRunLoopMode)
  }

  func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
    guard let inputStream = aStream as? InputStream else {
      return
    }
    switch eventCode {
    case Stream.Event.hasBytesAvailable:
      var buffer = [UInt8].init(repeating: 0, count: kAPAudioStreamReadMaxLength)
      let bytesRead = inputStream.read(&buffer, maxLength: kAPAudioStreamReadMaxLength)
      appendIdentityData(Data(bytes: UnsafePointer<UInt8>(buffer), count: bytesRead))
    case Stream.Event.endEncountered:
      disconnectSubject.onNext()
      disconnectSubject.onCompleted()
    default:
      return
    }
  }
  
  func close() {
    self.disposeBag = DisposeBag()
  }
  
  fileprivate func appendIdentityData(_ data: Data) {
    let stopPacketRange = data.range(of: NetworkingConstants.StopPacket, options: [], in: data.range(of: data))
    if let stopRange = stopPacketRange {
      if stopRange.lowerBound > 0 {
        let identityRange = Range(uncheckedBounds: (0, stopRange.lowerBound))
        identity.append(data.subdata(in: identityRange))
      }
      identifier.value = String(data: identity, encoding: String.Encoding.utf8)!
    }
    identity.append(data)
  }
  
  deinit {
    self.inputStream.close()
  }
}
