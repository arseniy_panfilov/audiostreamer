//
//  HostViewController.swift
//  TestMCP
//
//  Created by Arseniy on 05.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import UIKit
import MediaPlayer
import RxSwift
import RxCocoa

class HostViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, HostView {
  
  @IBOutlet weak var peersTable: UITableView! {
    didSet {
      peersTable.register(cellType: PeerCell.self)
    }
  }
  
  @IBOutlet weak var translateSwitch: UISwitch!
  
  fileprivate var cellModels: [PeerCellViewModel] = []
  
  lazy var shouldTranslate: Observable<Bool> = { [unowned self] in
    return self.translateSwitch.rx.value.asObservable()
  }()
  
  override func viewDidLoad() {
    peersTable.tableFooterView = UIView(frame: CGRect.zero)
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cellModels.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell: PeerCell = peersTable.dequeueReusableCell(for: indexPath)
    return cell
  }
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let cell = cell as? PeerCell else {
      return
    }
    
    cell.bind(cellModels[indexPath.row])
  }
  
  func updateClients(_ clients: [PeerCellViewModel]) {
    self.cellModels = clients
    peersTable.reloadData()
  }
 }
