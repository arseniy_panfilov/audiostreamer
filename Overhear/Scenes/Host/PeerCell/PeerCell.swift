//
//  ServiceCell.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import UIKit
import Reusable
import SnapKit

class PeerCell: UITableViewCell, NibReusable {
  
  @IBOutlet weak var serviceName: UILabel!
  
  func bind(_ viewModel: PeerCellViewModel) {
    self.serviceName.text = viewModel.serviceName
  }
}
