func load() {
    
}//
//  HostPresenter.swift
//  TestMCP
//
//  Created by Arseniy on 05.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import RxSwift
import MediaPlayer

protocol HostView {
  var shouldTranslate: Observable<Bool> { get }
  func updateClients(_ clients: [PeerCellViewModel])
}


class HostPresenter {
  
  let hostManager: AudioHostManager
  let disposeBag = DisposeBag()
  
  var view: HostView? {
    didSet {
      view?.shouldTranslate.bind {
        translate in
        if translate {
          self.hostManager.start()
        } else {
          self.hostManager.stop()
        }
      }.addDisposableTo(disposeBag)
    }
  }
  
  init(hostManager: AudioHostManager) {
    self.hostManager = hostManager
    
    hostManager.clients.asObservable().bind {
      clients in
      self.view?.updateClients(clients.map { PeerCellViewModel(withName: $0) })
    }.addDisposableTo(disposeBag)
  }
}
