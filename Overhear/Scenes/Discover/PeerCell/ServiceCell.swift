//
//  ServiceCell.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import UIKit
import Reusable
import SnapKit

class ServiceCell: UITableViewCell, NibReusable {
  
  @IBOutlet weak var serviceName: UILabel!
  @IBOutlet weak var connectionIndicator: UIView!
  
  func addBars() {
    for i in (0..<3) {
      let bar = createIndicatorBar()
      
      bar.frame.origin.x = CGFloat(i * 2) * bar.frame.width
      connectionIndicator.addSubview(bar)
      
      animateBar(bar, delay: Double(i) * 0.15)
    }
  }
  
  func animateBar(_ barView: UIView, delay: TimeInterval) {
    let morphAnimation = CABasicAnimation(keyPath: "bounds.size.height")
    morphAnimation.beginTime = CACurrentMediaTime() - delay
    morphAnimation.duration = 0.2
    morphAnimation.autoreverses = true
    
    morphAnimation.toValue = connectionIndicator.frame.height / 3.0
    morphAnimation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
    morphAnimation.repeatCount = Float.infinity
    morphAnimation.isRemovedOnCompletion = false
    
    barView.layer.position.y += barView.frame.height / 2.0
    barView.layer.anchorPoint = CGPoint(x: 0, y: 1.0)
    
    barView.layer.add(morphAnimation, forKey: nil)
  }
  
  fileprivate func createIndicatorBar() -> UIView {
    let barView = UIView(frame: connectionIndicator.frame)
    barView.frame.origin = CGPoint(x: 0,y: 0)
    barView.frame.size.width /= 5.0
    barView.backgroundColor = UIColor(red: 1.0, green: 45.0/255.0, blue: 85.0/255.0, alpha: 1.0)
    barView.layer.cornerRadius = 1.0
    
    return barView
  }
  
  func bind(_ viewModel: ServiceCellViewModel) {
    self.serviceName.text = viewModel.serviceName
    if viewModel.connected {
      addBars()
    }
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    self.connectionIndicator.subviews.forEach { $0.removeFromSuperview() }
  }
}
