//
//  ServiceCellViewModel.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation

class ServiceCellViewModel {
  let serviceName: String
  let connected: Bool
  
  init(withName name: String, connected: Bool) {
    self.serviceName = name
    self.connected = connected
  }
}