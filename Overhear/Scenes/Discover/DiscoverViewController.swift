//
//  DiscoverViewController.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import UIKit
import RxSwift


class DiscoverViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DiscoverView {
  
  fileprivate var cellModels: [ServiceCellViewModel] = []
  
  lazy var volume: Observable<Float> = { [unowned self] in
    self.volumeSlider.rx.value.asObservable()
  }()
  
  lazy var disconnect: Observable<Void> = { [unowned self] in
    self.disconnectButton.rx.tap.asObservable()
  }()
  
  @IBOutlet weak var volumeSlider: UISlider!
  @IBOutlet weak var volumeView: UIView!
  @IBOutlet weak var disconnectButton: UIButton!

  @IBOutlet weak var hostsTable: UITableView! {
    didSet {
      hostsTable.register(cellType: ServiceCell.self)
      hostsTable.tableFooterView = UIView(frame: CGRect.zero)
    }
  }
  
  let selectedRow = Variable<Int?>(nil)
  
  func updateTable(withData data: [ServiceCellViewModel]) {
    self.cellModels = data
    self.hostsTable.reloadData()
  }
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return cellModels.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return tableView.dequeueReusableCell(for: indexPath) as ServiceCell
  }
  
  
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard let serviceCell = cell as? ServiceCell else {
      return
    }
    
    serviceCell.bind(cellModels[indexPath.row])
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if selectedRow.value == indexPath.row {
      return
    }
    selectedRow.value = indexPath.row
  }
  
  func toggleConnectionControls(_ show: Bool?) {
    let show = show ?? volumeView.isHidden
    volumeView.isHidden = !show
    disconnectButton.isHidden = !show
  }
}
