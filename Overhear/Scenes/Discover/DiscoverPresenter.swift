//
//  DiscoverPresenter.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import RxSwift

protocol DiscoverView {
  func updateTable(withData data: [ServiceCellViewModel])
  func toggleConnectionControls(_ show: Bool?)
  
  var selectedRow: Variable<Int?> { get }
  
  var volume: Observable<Float> { get }
  var disconnect: Observable<Void> { get }
}

protocol AudioClient {
  func setVolume(_ volume: Float)
  
  var service: NetService { get }
}

class DiscoverPresenter {
  
  fileprivate let browser: ServiceBrowser
  fileprivate let disposeBag = DisposeBag()
  
  fileprivate let cellModels: Variable<[ServiceCellViewModel]> = Variable([])
  
  fileprivate var client: AudioClient?
  
  fileprivate var volume: Float = 0
  
  var view: DiscoverView? {
    didSet {
      view?.selectedRow.asObservable().filter { $0 != nil }.map { $0! }.bind {
        rowIndex in
        guard let client = self.browser.connectToService(withIndex: rowIndex) else {
          return
        }
        client.setVolume(self.volume)
        self.client = client
        self.view?.toggleConnectionControls(true)
        self.cellModels.value[rowIndex] = ServiceCellViewModel(withName: client.service.name, connected: true)
        
      }.addDisposableTo(disposeBag)
      
      view?.volume.bind {
        self.volume = $0
        self.client?.setVolume($0)
      }.addDisposableTo(disposeBag)
      
      view?.disconnect.bind {
        self.view?.toggleConnectionControls(false)
        self.client = nil
        guard let connectedCell = self.cellModels.value.filter({ $0.connected == true }).first else {
          return
        }
        
        if let index = self.cellModels.value.index(where: { $0 === connectedCell }) {
          self.cellModels.value[index] = ServiceCellViewModel(withName: connectedCell.serviceName, connected: false)
        }
        self.view?.selectedRow.value = nil
      }.addDisposableTo(disposeBag)
      self.browser.start()
    }
  }
  
  init(browser: ServiceBrowser) {
    self.browser = browser
    
    self.browser.services.asObservable().skip(1).bind {
      services in
      self.cellModels.value = services.map {
        service in
        let connected = self.client.flatMap { $0.service.name == service.name } ?? false
        return ServiceCellViewModel(withName: service.name, connected: connected)
      }
    }.addDisposableTo(disposeBag)
    
    self.cellModels.asObservable().bind {
      models in
      DispatchQueue.main.async {
        self.view?.updateTable(withData: models)
      }
    }.addDisposableTo(disposeBag)
  }
}
