//
//  AppDelegate.swift
//  TestMCP
//
//  Created by Arseniy on 31.05.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import UIKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  let coordinator = Coordinator()
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    
    coordinator.launchMainInterface()
        
    return true
  }

}

