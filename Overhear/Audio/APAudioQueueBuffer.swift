//
//  APAudioQueueBuffer.swift
//  TestMCP
//
//  Created by Arseniy on 06.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer

let kTDMaxPacketDescriptions: UInt32 = 512

func ==(lhs: APAudioQueueBuffer, rhs: AudioQueueBufferRef) -> Bool {
  return lhs.audioQueueBuffer == rhs
}

class APAudioQueueBuffer {
  var audioQueueBuffer: AudioQueueBufferRef? = nil
  var size: Int = 0
  var fillPosition: Int = 0
  var packetDescriptions: [AudioStreamPacketDescription] = []
  var numberOfPacketDescriptions: UInt32 {
    return UInt32(packetDescriptions.count)
  }
  
   init?(audioQueue: AudioQueueRef, size: Int) {
    self.size = size
    let err: OSStatus = AudioQueueAllocateBuffer(audioQueue, UInt32(self.size), &audioQueueBuffer)
    if err != 0 {
      return nil
    }
  }
  
  func fillWithData(_ data: Data, offset: Int) -> Int {
    let length = data.count
    if self.fillPosition + length <= self.size {
      let dest = self.audioQueueBuffer?.pointee.mAudioData.advanced(by: self.fillPosition)
      memcpy(dest, (data as NSData).bytes.advanced(by: offset), length)
      self.fillPosition += length
    }
    else {
      let availableSpace: Int = self.size - self.fillPosition
      let dest = self.audioQueueBuffer?.pointee.mAudioData.advanced(by: self.fillPosition)
      memcpy(dest, (data as NSData).bytes, availableSpace)
      self.fillPosition = self.size
      return length - availableSpace
    }
    if self.fillPosition == self.size {
      return -1
    }
    return 0
  }
  
  func fillWithData(_ data: Data, packetDescription: AudioStreamPacketDescription) -> Bool {
    let packetSize = Int(packetDescription.mDataByteSize)
    if self.fillPosition + packetSize > self.size || self.numberOfPacketDescriptions == kTDMaxPacketDescriptions {
      return false
    }

    memcpy(self.audioQueueBuffer?.pointee.mAudioData.advanced(by: self.fillPosition), (data as NSData).bytes, packetSize)

    var description = packetDescription
    description.mStartOffset = Int64(self.fillPosition)
    self.packetDescriptions.append(description)

    self.fillPosition += Int(packetDescription.mDataByteSize)
    return true
  }
  
  func enqueueWithAudioQueue(_ audioQueue: AudioQueueRef) {
    self.audioQueueBuffer?.pointee.mAudioDataByteSize = UInt32(self.fillPosition)
    AudioQueueEnqueueBuffer(audioQueue, self.audioQueueBuffer!, self.numberOfPacketDescriptions, self.packetDescriptions)
  }
  
  func reset() {
    self.fillPosition = 0
    self.packetDescriptions = []
  }
  
  func freeFromAudioQueue(_ audioQueue: AudioQueueRef) {
    AudioQueueFreeBuffer(audioQueue, self.audioQueueBuffer!)
  }
}
