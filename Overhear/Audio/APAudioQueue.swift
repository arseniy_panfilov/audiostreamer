//
//  APAudioQueue.swift
//  TestMCP
//
//  Created by Arseniy on 06.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer

enum APAudioQueueState : Int {
  case buffering
  case stopped
  case paused
  case playing
}

protocol APAudioQueueDelegate: class {
  func audioQueueDidFinishPlaying(_ audioQueue: APAudioQueue)
  func audioQueueDidStartPlaying(_ audioQueue: APAudioQueue)
}

func APAudioQueueOutputCallback(inUserData: UnsafeMutableRawPointer?, inAudioQueue: AudioQueueRef, inAudioQueueBuffer:AudioQueueBufferRef) {
  guard let userData = inUserData else {
    return
  }
  let audioQueue: APAudioQueue = bridge(userData)
  audioQueue.didFreeAudioQueueBuffer(inAudioQueueBuffer)
}

func APAudioQueuePropertyListener(userData: UnsafeMutableRawPointer?, audioQueue: AudioQueueRef, propertyID: AudioQueuePropertyID) {
  guard let userData = userData else { return }
  if propertyID == kAudioQueueProperty_IsRunning {
    var prop: UInt32 = 0
    var size = UInt32(MemoryLayout<UInt32>.size)
    AudioQueueGetProperty(audioQueue, propertyID, &prop, &size)
    let audioQueue: APAudioQueue = bridge(userData)
    if prop == 1 {
      audioQueue.delegate?.audioQueueDidStartPlaying(audioQueue)
    }
    else {
      audioQueue.delegate?.audioQueueDidFinishPlaying(audioQueue)
    }
  }
}


class APAudioQueue {
  
  var state: APAudioQueueState = .buffering
  weak var delegate: APAudioQueueDelegate?
  
  var audioQueue: AudioQueueRef? = nil
  var bufferManager: APAudioQueueBufferManager!
  var waitForFreeBufferCondition = NSCondition()
  var buffersToFillBeforeStart: Int = kAPAudioQueueStartMinimumBuffers
  let autoPlay: Bool
  
  init?(basicDescription: AudioStreamBasicDescription, bufferCount: Int, bufferSize: Int, magicCookieData: UnsafeMutablePointer<UInt8>?, magicCookieSize: UInt32, autoPlay: Bool) {
    self.autoPlay = autoPlay
    var basicDescription = basicDescription
    
    let bridgit = bridge(self)
    var status = AudioQueueNewOutput(&basicDescription, APAudioQueueOutputCallback as AudioQueueOutputCallback, bridgit, nil, nil, 0, &audioQueue)
    if status != 0 {
      return nil
    }
    
    AudioQueueAddPropertyListener(audioQueue!, kAudioQueueProperty_IsRunning, APAudioQueuePropertyListener as AudioQueuePropertyListenerProc, bridge(self))
    
    self.bufferManager = APAudioQueueBufferManager(audioQueue: self.audioQueue!, size: bufferSize, count: bufferCount)
    if let magicCookieData = magicCookieData {
      status = AudioQueueSetProperty(self.audioQueue!, kAudioQueueProperty_MagicCookie, magicCookieData, magicCookieSize)
    }
    status = AudioQueueSetParameter(self.audioQueue!, kAudioQueueParam_Volume, 1.0)
  }
  
  func setVolume(_ volume: Float) {
    AudioQueueSetParameter(audioQueue!, kAudioQueueParam_Volume, volume)
  }
  
  func didFreeAudioQueueBuffer(_ audioQueueBuffer: AudioQueueBufferRef) {
    self.bufferManager.freeAudioQueueBuffer(audioQueueBuffer)
    self.waitForFreeBufferCondition.lock()
    self.waitForFreeBufferCondition.signal()
    self.waitForFreeBufferCondition.unlock()
  }
  
  func nextFreeBuffer() -> APAudioQueueBuffer {
    if !self.bufferManager.hasAvailableAudioQueueBuffer {
      self.waitForFreeBufferCondition.lock()
      self.waitForFreeBufferCondition.wait()
      self.waitForFreeBufferCondition.unlock()
    }
    guard let nextBuffer: APAudioQueueBuffer = self.bufferManager.nextFreeBuffer() else {
      return self.nextFreeBuffer()
    }
    return nextBuffer
  }
  
  func enqueue() {
    self.bufferManager.enqueueNextBufferOnAudioQueue(self.audioQueue!)
    guard self.state == .buffering && autoPlay else {
      return
    }
    self.buffersToFillBeforeStart -= 1
    if self.buffersToFillBeforeStart == 0 {
      AudioQueuePrime(self.audioQueue!, 0, nil)
      self.play()
    }
  }
  
  func play() {
    if self.state == .playing {
      return
    }
    AudioQueueStart(audioQueue!, nil)
    self.state = .playing
  }
  
  func pause() {
    if self.state == .paused {
      return
    }
    AudioQueuePause(audioQueue!)
    self.state = .paused
  }
  
  func stop() {
    if self.state == .stopped {
      return
    }
    AudioQueueStop(audioQueue!, true) // immediately
    self.state = .stopped
  }
  
  func finish() {
    if self.state == .stopped {
      return
    }
    AudioQueueStop(audioQueue!, false) // not immediately
    
    self.state = .stopped
  }
  
  
  
  func fill(withData data: Data, packetDescription: AudioStreamPacketDescription) {
    let audioQueueBuffer: APAudioQueueBuffer = self.nextFreeBuffer()
    let hasMoreRoomForPackets: Bool = audioQueueBuffer.fillWithData(data, packetDescription: packetDescription)
    if !hasMoreRoomForPackets {
      self.enqueue()
      self.fill(withData: data, packetDescription: packetDescription)
    }
  }
  
  func fill(withData data: Data, offset: Int) {
    let length = data.count
    let audioQueueBuffer: APAudioQueueBuffer = self.nextFreeBuffer()
    let leftovers: Int = audioQueueBuffer.fillWithData(data, offset: offset)
    if leftovers == 0 {
      return
    }
    self.enqueue()
    if leftovers > 0 {
      self.fill(withData: data, offset: (length - leftovers))
    }
  }
  
  deinit {
    self.bufferManager.freeBufferMemoryFromAudioQueue(self.audioQueue!)
    AudioQueueDispose(self.audioQueue!, true)
  }
}
