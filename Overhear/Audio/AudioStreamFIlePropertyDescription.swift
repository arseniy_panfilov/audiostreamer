//
//  AudioStreamFIlePropertyDescription.swift
//  TestMCP
//
//  Created by Arseniy on 06.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer

extension AudioFormatPropertyID {
  var propertyDescription: String {
    switch self {
    case kAudioFileStreamProperty_DataFormat:
      return "kAudioFileStreamProperty_DataFormat"
    case kAudioFileStreamProperty_ReadyToProducePackets:
      return "kAudioFileStreamProperty_ReadyToProducePackets"
    case kAudioFileStreamProperty_FileFormat:
      return "kAudioFileStreamProperty_FileFormat"
    case kAudioFileStreamProperty_DataFormat:
      return "kAudioFileStreamProperty_DataFormat"
    case kAudioFileStreamProperty_FormatList:
      return "kAudioFileStreamProperty_FormatList"
    case kAudioFileStreamProperty_MagicCookieData:
      return "kAudioFileStreamProperty_MagicCookieData"
    case kAudioFileStreamProperty_AudioDataByteCount:
      return "kAudioFileStreamProperty_AudioDataByteCount"
    case kAudioFileStreamProperty_AudioDataPacketCount:
      return "kAudioFileStreamProperty_AudioDataPacketCount"
    case kAudioFileStreamProperty_MaximumPacketSize:
      return "kAudioFileStreamProperty_MaximumPacketSize"
    case kAudioFileStreamProperty_DataOffset:
      return "kAudioFileStreamProperty_DataOffset"
    case kAudioFileStreamProperty_ChannelLayout:
      return "kAudioFileStreamProperty_ChannelLayout"
    case kAudioFileStreamProperty_PacketToFrame:
      return "kAudioFileStreamProperty_PacketToFrame"
    case kAudioFileStreamProperty_FrameToPacket:
      return "kAudioFileStreamProperty_FrameToPacket"
    case kAudioFileStreamProperty_PacketToByte:
      return "kAudioFileStreamProperty_PacketToByte"
    case kAudioFileStreamProperty_ByteToPacket:
      return "kAudioFileStreamProperty_ByteToPacket"
    case kAudioFileStreamProperty_PacketTableInfo:
      return "kAudioFileStreamProperty_PacketTableInfo"
    case kAudioFileStreamProperty_PacketSizeUpperBound:
      return "kAudioFileStreamProperty_PacketSizeUpperBound"
    case kAudioFileStreamProperty_AverageBytesPerPacket:
      return "kAudioFileStreamProperty_AverageBytesPerPacket"
    case kAudioFileStreamProperty_BitRate:
      return "kAudioFileStreamProperty_BitRate"
    case kAudioFileStreamProperty_InfoDictionary:
      return "kAudioFileStreamProperty_InfoDictionary"
    default:
      return "Unkown property"
    }
  }
}
