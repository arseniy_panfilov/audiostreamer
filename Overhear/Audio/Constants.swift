//
//  Constants.swift
//  TestMCP
//
//  Created by Arseniy on 06.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation

let kAPAudioStreamReadMaxLength = 2048
let kAPAudioQueueBufferSize: Int = 2048 * 8
let kAPAudioQueueBufferCount = 8
let kAPAudioQueueStartMinimumBuffers: Int = 2
