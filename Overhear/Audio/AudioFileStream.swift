//
//  AudioFileStream.swift
//  TestMCP
//
//  Created by Arseniy on 05.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer


protocol AudioFileStreamDelegate: class {
  func audioFileStream(_ sender: AudioFileStream, didReceiveError error: OSStatus)
  func audioFileStream(_ sender: AudioFileStream, didReceiveData data: Data)
  func audioFileStream(_ sender: AudioFileStream, didReceiveData data: Data, packetDescription: AudioStreamPacketDescription)
  func audioFileStreamDidBecomeReady(_ sender: AudioFileStream)
}


class AudioFileStream {
  fileprivate var audioFileStreamID: AudioFileStreamID? = nil
  
  var basicDescription: AudioStreamBasicDescription = AudioStreamBasicDescription()
  fileprivate var totalByteCount: UInt64 = 0
  
  weak var delegate: AudioFileStreamDelegate?
  
  var packetBufferSize: UInt32 = 0
  var magicCookieData: UnsafeMutablePointer<UInt8>? = nil
  var magicCookieLength: UInt32 = 0
  var discontinuous = true
  
  let AudioFileStreamPropertyListener: AudioFileStream_PropertyListenerProc = {
    inClientData, inAudioFileStreamID, inPropertyID, ioFlags in
    let audioStream: AudioFileStream = bridge(inClientData)
    audioStream.didChangeProperty(inPropertyID, flags: ioFlags)
  }
  
  let AudioFileStreamPacketsListener: AudioFileStream_PacketsProc = {
    inClientData, inNumberBytes, inNumberPackets, inInputData, inPacketDescriptions in
    let myClass: AudioFileStream = bridge(inClientData)
    myClass.didReceivePackets(inInputData, packetDescriptions: inPacketDescriptions, numberOfPackets: inNumberPackets, numberOfBytes: inNumberBytes)
    return
  }
  
  init() {
    AudioFileStreamOpen(bridge(self), AudioFileStreamPropertyListener, AudioFileStreamPacketsListener, 0, &audioFileStreamID)
  }
  
  func parseData(_ data: Data) {
    var status: OSStatus = 0
    if self.discontinuous {
      status = AudioFileStreamParseBytes(audioFileStreamID!, UInt32(data.count), (data as NSData).bytes, AudioFileStreamParseFlags.discontinuity)
      self.discontinuous = false
    } else {
      status = AudioFileStreamParseBytes(audioFileStreamID!, UInt32(data.count), (data as NSData).bytes, [])
    }
    if status != 0 {
      self.delegate?.audioFileStream(self, didReceiveError: status)
    }
  }
  
  fileprivate func didChangeProperty(_ propertyID: AudioFileStreamPropertyID, flags: UnsafeMutablePointer<AudioFileStreamPropertyFlags>) {
    if (propertyID == kAudioFileStreamProperty_ReadyToProducePackets) {
    
      var status = readAudioStreamFileProperty(kAudioFileStreamProperty_DataFormat, nil, &basicDescription)
      
      if status != 0 {
        self.delegate?.audioFileStream(self, didReceiveError: status)
        return
      }
      
      status = readAudioStreamFileProperty(kAudioFileStreamProperty_AudioDataByteCount, nil, &totalByteCount)
      
      status = readAudioStreamFileProperty(kAudioFileStreamProperty_PacketSizeUpperBound, nil, &packetBufferSize)
      
      if status != 0 || self.packetBufferSize == 0 {
        status = readAudioStreamFileProperty(kAudioFileStreamProperty_MaximumPacketSize, nil, &packetBufferSize)
      }
      
      status = readAudioStreamFileProperty(kAudioFileStreamProperty_MagicCookieData, &magicCookieLength, &magicCookieData)
      
      delegate?.audioFileStreamDidBecomeReady(self)
    }
  }

  
  fileprivate func readAudioStreamFileProperty(_ property: AudioFileStreamPropertyID, _ ioSize: UnsafeMutablePointer<UInt32>?, _ data: UnsafeMutableRawPointer) -> OSStatus {
    
    var writable: DarwinBoolean = false
    var size: UInt32 = 0
    var status = AudioFileStreamGetPropertyInfo(self.audioFileStreamID!, property, &size, &writable)
    guard status == 0 else {
      return status
    }
    var actualSize = ioSize == nil ? size : ioSize?.pointee
    status = AudioFileStreamGetProperty(self.audioFileStreamID!, property, &actualSize!, data)
    guard status == 0 else {
      return status
    }
    return 0
  }
  
  fileprivate func didReceivePackets(_ packets: UnsafeRawPointer, packetDescriptions: UnsafeMutablePointer<AudioStreamPacketDescription>?, numberOfPackets: UInt32, numberOfBytes: UInt32) {
    if let packetDescriptions = packetDescriptions {
      for i in 0 ..< Int(numberOfPackets) {
        let packetOffset = Int(packetDescriptions[i].mStartOffset)
        let packetSize = Int(packetDescriptions[i].mDataByteSize)
        let data = Data(bytes: packets.advanced(by: packetOffset), count: packetSize)
        self.delegate?.audioFileStream(self, didReceiveData: data, packetDescription: packetDescriptions[i])
      }
    }
    else {
      let packetData = Data(bytes: packets, count: Int(numberOfBytes))
      delegate?.audioFileStream(self, didReceiveData: packetData)
    }
  }
  
  deinit {
    AudioFileStreamClose(self.audioFileStreamID!)
  }
}
