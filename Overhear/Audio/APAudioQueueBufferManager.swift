//
//  APAudioQueueBufferManager.swift
//  TestMCP
//
//  Created by Arseniy on 06.06.16.
//  Copyright © 2016 Arseniy Panfilov. All rights reserved.
//

import Foundation
import MediaPlayer

class APAudioQueueBufferManager {
  
  var bufferCount: Int
  var bufferSize: Int
  var audioQueueBuffers: [APAudioQueueBuffer]
  var freeBuffers: [Int]
  
  let lockQueue = DispatchQueue(label: "com.test.lockqueue", attributes: [])
  
  var hasAvailableAudioQueueBuffer: Bool { return freeBuffers.count > 0 }
  
  init(audioQueue: AudioQueueRef, size: Int, count: Int) {

    self.bufferCount = count
    self.bufferSize = size
    self.freeBuffers = []
    var audioqueuebuffers: [APAudioQueueBuffer] = []
    var i = 0
    while audioqueuebuffers.count < self.bufferCount {
      guard let buffer = APAudioQueueBuffer(audioQueue: audioQueue, size: self.bufferSize) else {
        continue
      }
      
      audioqueuebuffers.append(buffer)
      self.freeBuffers.append(i)
      i += 1
    }
    self.audioQueueBuffers = audioqueuebuffers
  }
  
  func freeAudioQueueBuffer(_ audioQueueBuffer: AudioQueueBufferRef) {
    for (index, buffer) in self.audioQueueBuffers.enumerated() {
      if buffer == audioQueueBuffer {
        buffer.reset()
      
        lockQueue.sync {
          self.freeBuffers.append(index)
        }
      }
    }
  }
  
  func nextFreeBuffer() -> APAudioQueueBuffer? {
    var result: APAudioQueueBuffer?
    lockQueue.sync {
      guard let index = self.freeBuffers.last else {
        result = nil
        return
      }
      result = self.audioQueueBuffers[index]
    }
    return result
  }
  
  func enqueueNextBufferOnAudioQueue(_ audioQueue: AudioQueueRef) {
    lockQueue.sync {
      guard let nextBufferIndex = self.freeBuffers.popLast() else {
        return
      }
      let nextBuffer: APAudioQueueBuffer = self.audioQueueBuffers[nextBufferIndex]
      nextBuffer.enqueueWithAudioQueue(audioQueue)
    }
  }

  
  func isProcessingAudioQueueBuffer() -> Bool {
    var result = false
    lockQueue.sync {
      result = self.freeBuffers.count != self.bufferCount
    }
    return result
  }
  
  func freeBufferMemoryFromAudioQueue(_ audioQueue: AudioQueueRef) {
    self.audioQueueBuffers.forEach { $0.freeFromAudioQueue(audioQueue) }
  }
}
